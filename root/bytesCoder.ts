import { sha256 } from '@noble/hashes/sha256';
import {
  base16,
  base32,
  base32crockford,
  base32hex,
  base58,
  base58check,
  base58flickr,
  base58xmr,
  base58xrp,
  base64,
  base64url,
  base64urlnopad,
  BytesCoder,
  hex,
  utf8,
} from '@scure/base';
import { sortObject } from 'rambdax';
import { sort } from 'rambdax/immutable';
import { naturalCompare } from './helper';

export const bytesCoderTypes = sort(naturalCompare, <const>[
  'base16',
  'base32',
  'base32crockford',
  'base32hex',
  'base58',
  'base58flickr',
  'base58xmr',
  'base58xrp',
  'base64',
  'base64url',
  'base64urlnopad',
  'hex',
  'utf8',
  'base58check',
]);

export type BytesCoderType = (typeof bytesCoderTypes)[number];

const _bytesCoderMap: Record<BytesCoderType, BytesCoder> = {
  base16,
  base32,
  base32crockford,
  base32hex,
  base58,
  base58flickr,
  base58xmr,
  base58xrp,
  base64,
  base64url,
  base64urlnopad,
  hex,
  utf8,
  base58check: base58check(sha256),
};

export const bytesCoderMap = <typeof _bytesCoderMap>sortObject<BytesCoder>(naturalCompare, _bytesCoderMap);
