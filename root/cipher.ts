import {
  _poly1305_aead,
  chacha12,
  chacha20orig,
  chacha20poly1305,
  chacha8,
  xchacha20poly1305,
} from '@noble/ciphers/chacha';
import { xsalsa20poly1305 } from '@noble/ciphers/salsa';
import {
  aes_128_cbc,
  aes_128_ctr,
  aes_128_gcm,
  aes_256_cbc,
  aes_256_ctr,
  aes_256_gcm,
} from '@noble/ciphers/webcrypto/aes';
import { BinaryFF1 } from '@noble/ciphers/webcrypto/ff1';
import { sortObject } from 'rambdax';
import { sort } from 'rambdax/immutable';
import { naturalCompare } from './helper';
import { AsyncCipherFunction, CipherFunction } from './types';

export const chacha20origpoly1305 = _poly1305_aead(chacha20orig);
export const chacha8poly1305 = _poly1305_aead(chacha8);
export const chacha12poly1305 = _poly1305_aead(chacha12);

export const cipherTypes = sort(naturalCompare, <const>[
  'chacha20origpoly1305',
  'chacha20poly1305',
  'chacha8poly1305',
  'xchacha20poly1305',
  'chacha12poly1305',
  'xsalsa20poly1305',
]);

export type CipherType = (typeof cipherTypes)[number];

const _cipherMap: Record<CipherType, CipherFunction> = {
  chacha20origpoly1305,
  chacha20poly1305,
  xchacha20poly1305,
  chacha8poly1305,
  chacha12poly1305,
  xsalsa20poly1305,
};

export const cipherMap = <typeof _cipherMap>sortObject<CipherFunction>(naturalCompare, _cipherMap);

export const asyncChipherTypes = sort(naturalCompare, <const>[
  'aes_128_cbc',
  'aes_128_ctr',
  'aes_128_gcm',
  'aes_256_cbc',
  'aes_256_ctr',
  'aes_256_gcm',
  'BinaryFF1',
]);

export type AsyncChipherType = (typeof asyncChipherTypes)[number];

const _asyncChipherMap: Record<AsyncChipherType, AsyncCipherFunction> = {
  aes_128_cbc,
  aes_128_ctr,
  aes_128_gcm,
  aes_256_cbc,
  aes_256_ctr,
  aes_256_gcm,
  BinaryFF1,
};

export const asyncChipherMap = <typeof _asyncChipherMap>(
  sortObject<AsyncCipherFunction>(naturalCompare, _asyncChipherMap)
);
