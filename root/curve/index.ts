import { sortObject } from 'rambdax';
import { sort } from 'rambdax/immutable';
import { naturalCompare } from '../helper';
import { Curve } from '../types';
import { bls12_381 } from './bls12_381';
import { bn254 } from './bn254';
import { ed25519, ed25519ctx, ed25519ph } from './ed25519';
import { ed448, ed448ph } from './ed448';
import { jubjub } from './jubjub';
import { p256, secp256r1 } from './p256';
import { p384, secp384r1 } from './p384';
import { p521, secp521r1 } from './p521';
import { pallas, vesta } from './pasta';
import { schnorr, secp256k1 } from './secp256k1';

export * from './abstract';

export const curveTypes = sort(naturalCompare, <const>[
  'bls12_381',
  'bn254',
  'ed448',
  'ed448ph',
  'ed25519',
  'ed25519ctx',
  'ed25519ph',
  'jubjub',
  'p256',
  'secp256r1',
  'p384',
  'secp384r1',
  'p521',
  'secp521r1',
  'pallas',
  'vesta',
  'schnorr',
  'secp256k1',
]);

export type CurveType = (typeof curveTypes)[number];

const _curveMap: Record<CurveType, Curve> = {
  bls12_381,
  bn254,
  ed448,
  ed448ph,
  ed25519,
  ed25519ctx,
  ed25519ph,
  jubjub,
  p256,
  secp256r1,
  p384,
  secp384r1,
  p521,
  secp521r1,
  pallas,
  vesta,
  schnorr,
  secp256k1,
};

export const curveMap = <typeof _curveMap>sortObject(naturalCompare, _curveMap);
