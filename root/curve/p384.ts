import { p384 as p384Curve, secp384r1 as secp384r1Curve } from '@noble/curves/p384';
import { makeWeierstrassCurve } from './abstract';

const keySize = 48;
export const p384 = makeWeierstrassCurve(p384Curve, keySize);
export const secp384r1 = makeWeierstrassCurve(secp384r1Curve, keySize);
