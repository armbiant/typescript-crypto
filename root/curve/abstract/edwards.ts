import { CurveFn } from '@noble/curves/abstract/edwards';
import { Hex } from '@noble/curves/abstract/utils';
import { Curve } from '../../types';

export const makeEdwardsCurve = (curve: CurveFn, keySize: number): Curve => ({
  keySize,
  randomPrivateKey: curve.utils.randomPrivateKey,
  getPublicKey: (privateKey: Hex, isCompressed?: boolean): Uint8Array => curve.getPublicKey(privateKey),
  sign: curve.sign,
  verify: curve.verify,
});
