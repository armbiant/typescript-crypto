import { bn254 as bn254Curve } from '@noble/curves/bn254';
import { makeWeierstrassCurve } from './abstract';

export const bn254 = makeWeierstrassCurve(bn254Curve, 32);
