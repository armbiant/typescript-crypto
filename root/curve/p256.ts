import { p256 as p256Curve, secp256r1 as secp256r1Curve } from '@noble/curves/p256';
import { makeWeierstrassCurve } from './abstract';

const keySize = 32;
export const p256 = makeWeierstrassCurve(p256Curve, keySize);
export const secp256r1 = makeWeierstrassCurve(secp256r1Curve, keySize);
