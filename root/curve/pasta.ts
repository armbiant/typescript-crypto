import { pallas as pallasCurve, vesta as vestaCurve } from '@noble/curves/pasta';
import { makeWeierstrassCurve } from './abstract';

const keySize = 32;
export const pallas = makeWeierstrassCurve(pallasCurve, keySize);
export const vesta = makeWeierstrassCurve(vestaCurve, keySize);
