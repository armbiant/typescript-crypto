import {
  ed25519 as ed25519Curve,
  ed25519ctx as ed25519ctxCurve,
  ed25519ph as ed25519phCurve,
} from '@noble/curves/ed25519';
import { makeEdwardsCurve } from './abstract';

const keySize = 32;
export const ed25519 = makeEdwardsCurve(ed25519Curve, keySize);
export const ed25519ctx = makeEdwardsCurve(ed25519ctxCurve, keySize);
export const ed25519ph = makeEdwardsCurve(ed25519phCurve, keySize);
