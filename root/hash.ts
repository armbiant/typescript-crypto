import { argon2d, argon2i, argon2id } from '@noble/hashes/argon2';
import { blake2b } from '@noble/hashes/blake2b';
import { blake2s } from '@noble/hashes/blake2s';
import { blake3 } from '@noble/hashes/blake3';
import { pbkdf2, scrypt } from '@noble/hashes/eskdf';
import { ripemd160 } from '@noble/hashes/ripemd160';
import { sha1 } from '@noble/hashes/sha1';
import { sha224, sha256 } from '@noble/hashes/sha256';
import {
  keccak_224,
  keccak_256,
  keccak_384,
  keccak_512,
  sha3_224,
  sha3_256,
  sha3_384,
  sha3_512,
  shake128,
  shake256,
} from '@noble/hashes/sha3';
import {
  cshake128,
  cshake256,
  k12,
  m14,
  parallelhash128,
  parallelhash128xof,
  parallelhash256,
  parallelhash256xof,
  tuplehash128,
  tuplehash128xof,
  tuplehash256,
  tuplehash256xof,
} from '@noble/hashes/sha3-addons';
import { sha384, sha512, sha512_224, sha512_256 } from '@noble/hashes/sha512';
import { CHash } from '@noble/hashes/utils';
import { sortObject } from 'rambdax';
import { sort } from 'rambdax/immutable';
import { naturalCompare } from './helper';
import { Argon2HashFunction, HashFunction, SaltHashFunction, TupleHashFunction } from './types';

export const hashWithoutOptionsTypes = sort(naturalCompare, <const>[
  'ripemd160',
  'sha1',
  'keccak_224',
  'keccak_256',
  'keccak_384',
  'keccak_512',
  'sha3_224',
  'sha3_256',
  'sha3_384',
  'sha3_512',
  'sha224',
  'sha256',
  'sha384',
  'sha512',
  'sha512_224',
  'sha512_256',
]);

export type HashWithoutOptionsType = (typeof hashWithoutOptionsTypes)[number];

const _hashWithoutOptionsMap: Record<HashWithoutOptionsType, CHash> = {
  ripemd160,
  sha1,
  keccak_224,
  keccak_256,
  keccak_384,
  keccak_512,
  sha3_224,
  sha3_256,
  sha3_384,
  sha3_512,
  sha224,
  sha256,
  sha384,
  sha512,
  sha512_224,
  sha512_256,
};

export const hashWithoutOptionsMap = <typeof _hashWithoutOptionsMap>(
  sortObject<CHash>(naturalCompare, _hashWithoutOptionsMap)
);

export const hashTypes = sort(naturalCompare, <const>[
  ...hashWithoutOptionsTypes,
  'blake2b',
  'blake2s',
  'blake3',
  'cshake128',
  'cshake256',
  'k12',
  'm14',
  'parallelhash128',
  'parallelhash128xof',
  'parallelhash256',
  'parallelhash256xof',
  'shake128',
  'shake256',
]);

export type HashType = (typeof hashTypes)[number];

const _hashMap: Record<HashType, HashFunction> = {
  blake2b,
  blake2s,
  blake3,
  ripemd160,
  sha1,
  cshake128,
  cshake256,
  k12,
  m14,
  parallelhash128,
  parallelhash128xof,
  parallelhash256,
  parallelhash256xof,
  keccak_224,
  keccak_256,
  keccak_384,
  keccak_512,
  sha3_224,
  sha3_256,
  sha3_384,
  sha3_512,
  shake128,
  shake256,
  sha224,
  sha256,
  sha384,
  sha512,
  sha512_224,
  sha512_256,
};

export const hashMap = <typeof _hashMap>sortObject<HashFunction>(naturalCompare, _hashMap);

export const tupleHashTypes = sort(naturalCompare, <const>[
  'tuplehash128',
  'tuplehash256',
  'tuplehash128xof',
  'tuplehash256xof',
]);

export type TupleHashType = (typeof tupleHashTypes)[number];

const _tupleHashMap: Record<TupleHashType, TupleHashFunction> = {
  tuplehash128,
  tuplehash256,
  tuplehash128xof,
  tuplehash256xof,
};

export const tupleHashMap = <typeof _tupleHashMap>sortObject<TupleHashFunction>(naturalCompare, _tupleHashMap);

export const saltHashTypes = sort(naturalCompare, <const>['scrypt', 'pbkdf2']);

export type SaltHashType = (typeof saltHashTypes)[number];

const _saltHashMap: Record<SaltHashType, SaltHashFunction> = { scrypt, pbkdf2 };

export const saltHashMap = <typeof _saltHashMap>sortObject<SaltHashFunction>(naturalCompare, _saltHashMap);

export const argon2HashTypes = sort(naturalCompare, <const>['argon2d', 'argon2i', 'argon2id']);

export type Argon2HashType = (typeof argon2HashTypes)[number];

const _argon2HashMap: Record<Argon2HashType, Argon2HashFunction> = { argon2d, argon2i, argon2id };

export const argon2HashMap = <typeof _argon2HashMap>sortObject<Argon2HashFunction>(naturalCompare, _argon2HashMap);
