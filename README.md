# crypto

Use [noble cryptography](https://paulmillr.com/noble/) packages to build list:

- hash:
  - hashMap
  - hashWithoutOptionsMap
  - tupleHashMap
  - saltHashMap
- cipher:
  - cipherMap
  - asyncChipherMap
- curve: curveMap
- bytesCoder: bytesCoderMap
